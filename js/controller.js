let DomID = (id) => {
  return document.getElementById(id);
};
let layThongTinTuForm = () => {
  let ma = DomID("ma").value;
  let name = DomID("name").value;
  let email = DomID("email").value;
  let address = DomID("address").value;
  let type = DomID("type").value;
  let toan = DomID("toan").value;
  let ly = DomID("ly").value;
  let hoa = DomID("hoa").value;
  let date = DomID("date").value;
  let salary = DomID("salary").value;
  let company = DomID("company").value;
  let hoaDon = DomID("hoaDon").value;
  let danhGia = DomID("danhGia").value;
  if (type == "Sinh Viên") {
    return new Student(ma, name, address, email, type, toan, ly, hoa);
  } else if (type == "Giáo Viên") {
    return new Employee(ma, name, address, email, type, date, salary);
  } else if (type == "Khách Hàng") {
    return new Customer(
      ma,
      name,
      address,
      email,
      type,
      company,
      hoaDon,
      danhGia
    );
  }
};

let renderList = (ListPerson) => {
  let contentHTML = "";
  let tinhNang =0
  ListPerson.forEach((item) => {
    if (item.type=="Sinh Viên") {
      tinhNang = 'Điểm Trung Bình : '+item.calculateDTB()
    }
    else if (item.type == "Giáo Viên") {
      tinhNang = 'Lương : ' +item.calculateLuong().toLocaleString() + 'VNĐ'
    }
    else if (item.type=="Khách Hàng") {
      tinhNang = 'Đánh Giá : '+ item.danhGia
        
    }
    let {ma,hoTen,email,address,type} = item
    
    let content = `<tr>
        <td>${ma}</td>
        <td>${hoTen}</td>
        <td>${email}</td>
        <td>${address}</td>
        <td>${type}</td>
        <td>${tinhNang}</td>
        <td><button class="btn btn-danger" onclick="Xoa('${ma}')">Xóa</button></td>
        <td><button class="btn btn-success" onclick="Sua('${ma}')">Sửa</button></td>
        </tr>`;
    contentHTML += content;
  });
  document.getElementById("tBodySP").innerHTML = contentHTML;
};
let showThongTinlenForm = (person) => {
  DomID("ma").value = person.ma;
  DomID("name").value = person.hoTen;
  DomID("email").value = person.email;
  DomID("address").value = person.address;
  DomID("type").value = person.type;
  if (person.type == "Sinh Viên") {
    document.getElementById("GiaoVien").style.display = "none"
    document.getElementById("SinhVien").style.display = "block"
    document.getElementById("KhachHang").style.display = "none"
    DomID("toan").value = person.math;
    DomID("ly").value = person.physical;
    DomID("hoa").value = person.chemistry;
    DomID("company").value = ''
    DomID("hoaDon").value = ''
    DomID("danhGia").value = ''
    DomID("date").value = '';
    DomID("salary").value = '';

  } else if (person.type == "Giáo Viên") {
    document.getElementById("GiaoVien").style.display = "block"
    document.getElementById("SinhVien").style.display = "none"
    document.getElementById("KhachHang").style.display = "none"
    DomID("date").value = person.date;
    DomID("salary").value = person.luongtheongay;
    DomID("toan").value = '';
    DomID("ly").value = '';
    DomID("hoa").value = '';
    DomID("company").value = ''
    DomID("hoaDon").value = ''
    DomID("danhGia").value = ''
  } else if (person.type = "Khách Hàng") {
    document.getElementById("GiaoVien").style.display = "none"
    document.getElementById("SinhVien").style.display = "none"
    document.getElementById("KhachHang").style.display = "block"
    DomID("company").value = person.tenCongTy;
    DomID("hoaDon").value = person.triGiaHoaDon;
    DomID("danhGia").value = person.danhGia;
    DomID("toan").value = '';
    DomID("ly").value = '';
    DomID("hoa").value = '';
    DomID("date").value = '';
    DomID("salary").value = '';
  }
};
