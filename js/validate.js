let showMessage =(idSpan,message)  =>{
    DomID(idSpan).innerText = message

}

class Validate {
    ktrTruonngRong (value,idSpan) {
        if (value=='') {
            showMessage(idSpan,'*Trường này không được để rỗng')
            return false
        }
        else {
            showMessage(idSpan,'')
            return true
        }
              
    }
    ktrDodai (value,idSpan) {
        if (value.length<=5) {
            showMessage(idSpan,"")
            return true
        }
        else {
            showMessage(idSpan,"*Mã chỉ gồm 5 ký tự")
            return false            
        }

    }
    ktrTrungTaikhoan (value,listNV,idSpan) {
        let index = listNV.findIndex(function(item) {
             return value == item.ma
         })
         if (index!=-1){
             showMessage(idSpan,"*Tài khoản này đã tồn tại")
             return false
         }
         else{
             showMessage(idSpan,"")
             return true
         }
    }
    ktrKhoangTrang (value,idSpan){
        let regex = /^[a-zA-Z0-9_.-]*$/
        if (regex.test(value)) {
            showMessage(idSpan,"")
            return true
        }
        else{
            showMessage(idSpan,"*Tài khoản không được chứa khoảng cách và ký tượng đặc biệt")
            return false
        }
    }
    ktrChu (value,idSpan,message){
        let regexChu = /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
        let regexKyTuong = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g
           if (regexChu.test(value) && !regexKyTuong.test(value)) {
               showMessage(idSpan,"")
               return true
           } else {
               showMessage(idSpan,message)
               return false
           }
    }
    ktrEmail (value,idSpan) {
        let re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (re.test(value)) {
            showMessage(idSpan,"")
            return true
        }
        else{
            showMessage(idSpan,"*Email chưa đúng định dạng")
            return false
        }
    }
    ktrLuaChon (value,idSpan,err) {
        if (value==err) {
            showMessage(idSpan,"*Phải chọn 1 trong 3")
            return false
        }
        else {
            showMessage(idSpan,"")
            return true
        }
    }
    ktrMinMax (value,min,max,idSpan,text) {
        if (value<=max && value>=min) {
            showMessage(idSpan,"")
            return true
        }
        else {
            showMessage(idSpan,`${text} phải từ ${min.toLocaleString()}-${max.toLocaleString()}`)
            return false
        }
    }
}
let valid = new Validate()
let validateMa = () => {
    let ma = DomID("ma").value
    let isValid = valid.ktrTruonngRong(ma,"tk") 
    && valid.ktrTrungTaikhoan(ma,PersonelManager1.ListPerson,"tk") 
    && valid.ktrKhoangTrang(ma,'tk')     
    && valid.ktrDodai(ma,'tk') 
    return isValid

}
let validateName = () => {
    let name = DomID("name").value
    let isValid = valid.ktrTruonngRong(name,"ten") 
    && valid.ktrChu(name,'ten',"*Tên phải là chữ")
    return isValid

}
let validateEmail = () => {
    let email = DomID("email").value
    let isValid = valid.ktrTruonngRong(email,"Email") && valid.ktrEmail(email,"Email")
    return isValid
}
let validateDiachi = () =>{
    let diachi = DomID("address").value
    let isValid = valid.ktrTruonngRong(diachi,"diachi") && valid.ktrChu(diachi,"diachi",'*Địa chỉ phải là chữ')
    return isValid

}
let validateLuaChon = () => {
    let type = DomID("type").value
    let isValid = valid.ktrLuaChon(type,"loai","none")
    return isValid
}
let validateSinhVien = () => {
    let toan = DomID('toan').value
    let ly = DomID("ly").value
    let hoa = DomID("hoa").value
    let isValid = (valid.ktrTruonngRong(toan,'math') && valid.ktrMinMax(toan,1,10,"math","*Điểm")) 
    & (valid.ktrTruonngRong(ly,'physical') && valid.ktrMinMax(ly,1,10,"physical","*Điểm"))
    & (valid.ktrTruonngRong(hoa,'chemistry') && valid.ktrMinMax(hoa,1,10,"chemistry","*Điểm"))
    return isValid

}
let validateGiaoVien = () => {
    let ngay = DomID("date").value
    let luong = DomID('salary').value
    let isValid =(valid.ktrTruonngRong(ngay,'ngay') && valid.ktrMinMax(ngay,0,31,"ngay","*Ngày")) 
    & (valid.ktrTruonngRong(luong,'luong') && valid.ktrMinMax(luong,200000,600000,'luong',"*Lương"))
    return isValid
} 
let validateKhachHang = () => {
    let congTy = DomID("company").value
    let hoaDon = DomID("hoaDon").value
    let danhGia = DomID("danhGia").value
    let isValid = (valid.ktrTruonngRong(congTy,"congty") & (valid.ktrTruonngRong(hoaDon,'hoadon')&& valid.ktrMinMax(hoaDon,1e6,2e6,'hoadon',"*Trị Giá Hóa Đơn"))
    & valid.ktrTruonngRong(danhGia,'danhgia'))
    return isValid
}
let isValidThem =(type) => {
    let validateSelect = null
    if (type == "Sinh Viên"){
        validateSelect = validateSinhVien()
    }
   else if (type == "Giáo Viên"){
        validateSelect = validateGiaoVien()
    }
    else if (type == "Khách Hàng"){
        validateSelect = validateKhachHang()
    }
    
    let isValid = validateMa() 
    &  validateName() 
    & validateEmail() 
    & validateDiachi() 
    & validateLuaChon()
    & validateSelect
    if (isValid) {
        return true
    }
    else {
        return false
    }
}
let isValidUpDate = (type) => {
    let validateSelect = null
    if (type == "Sinh Viên"){
        validateSelect = validateSinhVien()
    }
   else if (type == "Giáo Viên"){
        validateSelect = validateGiaoVien()
    }
    else if (type == "Khách Hàng"){
        validateSelect = validateKhachHang()
    }
    let isValid = validateName() 
    & validateEmail() 
    & validateDiachi() 
    & validateLuaChon()
    & (validateSelect)
    if (isValid) {
        return true
    }
    else {
        return false
    }

}