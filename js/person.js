// lớp person
class Person {
    constructor (ma,hoTen,address,email,type) {
        this.ma = ma 
        this.hoTen = hoTen,
        this.address = address,
        this.email = email;
        this.type = type;
    }

} 
class Student extends Person {
    constructor(ma,hoTen,address,email,type,math,physical,chemistry) {
        super (ma,hoTen,address,email,type)
        this.math = math,
        this.physical=  physical,
        this.chemistry = chemistry;
        
    }

} class Employee extends Person {
    constructor(ma,hoTen,address,email,type,date,luongtheongay) {
        super (ma,hoTen,address,email,type)
        this.date = date,
        this.luongtheongay = luongtheongay
        
    }
}
 class Customer extends Person {
    constructor(ma,hoTen,address,email,type,tenCongTy,triGiaHoaDon,danhGia) {
        super (ma,hoTen,address,email,type) 
        this.tenCongTy = tenCongTy,
        this.triGiaHoaDon = triGiaHoaDon,
        this.danhGia = danhGia
    }

}
